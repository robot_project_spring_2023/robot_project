clc
clear
%Forward Kinematic Calculation
Angles = [0 100 280 65 10 210 210];

t1 = -pi/180*Angles(1);
t2 = pi/180*Angles(2);
t3 = -pi/180*Angles(3);
t4 = pi/180*Angles(4);
t5 = -pi/180*Angles(5);
t6 = pi/180*Angles(6);
t7 = -pi/180*Angles(7);

f1=t1+pi
f2=t2+pi
f3=t3+pi
f4=t4+pi
f5=t5+pi
f6=t6+pi
f7=t7+pi


RZ1=[cos(t1+pi),-sin(t1+pi),0, 0;sin(t1+pi),cos(t1+pi),0,0;0,0,1,0;0,0,0,1];
TZ1=[1,0,0,0;0,1,0,0;0,0,1,0.2755;0,0,0,1];
TX1=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX1=[1,0,0,0;0,cos(-pi/2),-sin(-pi/2),0;0,sin(-pi/2),cos(-pi/2),0;0,0,0,1];
H0_1=RZ1*TZ1*TX1*RX1;

RZ2=[cos(t2+pi),-sin(t2+pi),0, 0;sin(t2+pi),cos(t2+pi),0,0;0,0,1,0;0,0,0,1];
TZ2=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
TX2=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX2=[1,0,0,0;0,cos(pi/2),-sin(pi/2),0;0,sin(pi/2),cos(pi/2),0;0,0,0,1];
H1_2=RZ2*TZ2*TX2*RX2;

RZ3=[cos(t3+pi),-sin(t3+pi),0, 0;sin(t3+pi),cos(t3+pi),0,0;0,0,1,0;0,0,0,1];
TZ3=[1,0,0,0;0,1,0,0;0,0,1,0.410;0,0,0,1];
TX3=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX3=[1,0,0,0;0,cos(-pi/2),-sin(-pi/2),0;0,sin(-pi/2),cos(-pi/2),0;0,0,0,1];
H2_3=RZ3*TZ3*TX3*RX3;

RZ4=[cos(t4+pi),-sin(t4+pi),0, 0;sin(t4+pi),cos(t4+pi),0,0;0,0,1,0;0,0,0,1];
TZ4=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
TX4=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX4=[1,0,0,0;0,cos(pi/2),-sin(pi/2),0;0,sin(pi/2),cos(pi/2),0;0,0,0,1];
H3_4=RZ4*TZ4*TX4*RX4;

RZ5=[cos(t5+pi),-sin(t5+pi),0, 0;sin(t5+pi),cos(t5+pi),0,0;0,0,1,0;0,0,0,1];
TZ5=[1,0,0,0;0,1,0,0;0,0,1,0.3111;0,0,0,1];
TX5=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX5=[1,0,0,0;0,cos(-pi/2),-sin(-pi/2),0;0,sin(-pi/2),cos(-pi/2),0;0,0,0,1];
H4_5=RZ5*TZ5*TX5*RX5;

RZ6=[cos(t6+pi),-sin(t6+pi),0, 0;sin(t6+pi),cos(t6+pi),0,0;0,0,1,0;0,0,0,1];
TZ6=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
TX6=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX6=[1,0,0,0;0,cos(pi/2),-sin(pi/2),0;0,sin(pi/2),cos(pi/2),0;0,0,0,1];
H5_6=RZ6*TZ6*TX6*RX6;

RZ7=[cos(t7+pi),-sin(t7+pi),0, 0;sin(t7+pi),cos(t7+pi),0,0;0,0,1,0;0,0,0,1];
TZ7=[1,0,0,0;0,1,0,0;0,0,1,0.2638;0,0,0,1];
TX7=[1,0,0,0;0,1,0,0;0,0,1,0;0,0,0,1];
RX7=[1,0,0,0;0,cos(0),-sin(0),0;0,sin(0),cos(0),0;0,0,0,1];
H6_7=RZ7*TZ7*TX7*RX7;

H0_7=H0_1*H1_2*H2_3*H3_4*H4_5*H5_6*H6_7


%Jacobian Matrix Calculation
O0_0 = [0;0;0];
Z0_0 = [0;0;1];

H0_1=H0_1;
Z0_1 = H0_1(1:3, 3);
O0_1 = H0_1(1:3, 4);

H0_2=H0_1*H1_2;
Z0_2 = H0_2(1:3, 3);
O0_2 = H0_2(1:3, 4);

H0_3=H0_2*H2_3;
Z0_3 = H0_3(1:3, 3);
O0_3 = H0_3(1:3, 4);

H0_4=H0_3*H3_4;
Z0_4 = H0_4(1:3, 3);
O0_4 = H0_4(1:3, 4);

H0_5=H0_4*H4_5;
Z0_5 = H0_5(1:3, 3);
O0_5 = H0_5(1:3, 4);

H0_6=H0_5*H5_6;
Z0_6 = H0_6(1:3, 3);
O0_6 = H0_6(1:3, 4);

H0_7=H0_6*H6_7;
Z0_7 = H0_7(1:3, 3);
O0_7 = H0_7(1:3, 4);

Jv1 = cross(Z0_0, (O0_7-O0_0)); 
Jv2 = cross(Z0_1, (O0_7-O0_1)); 
Jv3 = cross(Z0_2, (O0_7-O0_2)); 
Jv4 = cross(Z0_3, (O0_7-O0_3));
Jv5 = cross(Z0_4, (O0_7-O0_4));
Jv6 = cross(Z0_5, (O0_7-O0_5));
Jv7 = cross(Z0_6, (O0_7-O0_6));

Jv = cat(2, Jv1, Jv2, Jv3, Jv4, Jv5, Jv6, Jv7);
Jw = cat(2, Z0_0, Z0_1, Z0_2, Z0_3, Z0_4, Z0_5, Z0_6);
J = cat(1, Jv, Jw)

%Singularity Detection
r = rank(J)
m=min(6,7)

if r < min(6,7)
    fprintf("The Jacobian Matrix is Singular")
else
    fprintf("The Jacobian Matrix is not Singular")
end

clc
clear
% Define the initial and final conditions
theta1_initial = 0;
theta2_initial = 0;
theta1_final = pi/2;
theta2_final = pi/2;

% Define the obstacle location
obstacle_x = 2;
obstacle_y = 0.5;

% Define the step size for gradient descent
step_size = 0.01;

% Define the robot's link lengths
link_length1 = 1;
link_length2 = 1;

% Define the repulsive and attractive field constants
repulsive_constant = 0.5;
attractive_constant = 2;

% Define the robot's points on link 1 and link 2
o1_initial = [1; 0];
o2_initial = [2; 0];
o1_final = [0; 1];
o2_final = [-1; 1];

% Initialize the robot's configuration
theta1 = theta1_initial;
theta2 = theta2_initial;

% Initialize the robot's configurations array
robot_configurations = [theta1_initial; theta2_initial];

% Perform Potential Field to find the path
while (abs(theta1 - theta1_final) > 0.01) || (abs(theta2 - theta2_final) > 0.01)
    % Calculate the attractive field
    attractive_o1 = attractive_constant * (theta1_final - theta1);
    attractive_o2 = attractive_constant * (theta2_final - theta2);
    
    % Intialize the repulsive field
    repulsive_o1 = 0;
    repulsive_o2 = 0;
    
    % Update the robot's points on link 1 and link 2
    o1 = [link_length1 * cos(theta1); link_length1 * sin(theta1)];
    o2 = [link_length1 * cos(theta1) + link_length2 * cos(theta1 + theta2); link_length1 * sin(theta1) + link_length2 * sin(theta1 + theta2)];
    
    % Calculate the distances between the robot's points and the obstacle
    distance1 = sqrt((obstacle_x - o1(1))^2 + (obstacle_y - o1(2))^2);
    distance2 = sqrt((obstacle_x - o2(1))^2 + (obstacle_y - o2(2))^2);
    
    % Check if the robot is within the obstacle's radius for any of the points
    if distance1 < 0.3 || distance2 < 0.3
        % Calculate the repulsive field for the point on link 1
        if distance1 < 0.3
            repulsive_o1 = repulsive_constant * 1/norm(o1)^2*o1/norm(o1);
        end
        
        % Calculate the repulsive field for the point on link 2
        if distance2 < 0.3
             repulsive_o2= repulsive_constant * 1/norm(o2)^2*o2/norm(o2);
        end
    end
    
    % Update the robot's configuration by calculating the torque of
    % both joints via the jacobian matrices of o1 and o2
    Jo1=[-sin(theta1),0;cos(theta1),0];
    Jo2=[-sin(theta1)-sin(theta1+theta2),-sin(theta1+theta2);cos(theta1)+cos(theta1+theta2),cos(theta1+theta2)];
    torques=[attractive_o1 + repulsive_o1;attractive_o2 + repulsive_o2];
    torque=Jo1'*torques(1)+Jo2'*torques(2);
    theta1 = theta1 + step_size * (torques(1));
    theta2 = theta2 + step_size * (torques(2));
    
    % Store the robot's configuration for plotting
    robot_configurations = [robot_configurations, [theta1; theta2]];
    
    % Update the robot's points on link 1 and link 2
    o1 = [link_length1 * cos(theta1); link_length1 * sin(theta1)];
    o2 = [link_length1 * cos(theta1) + link_length2 * cos(theta1 + theta2); link_length1 * sin(theta1) + link_length2 * sin(theta1 + theta2)];

    % Plot the robot's current position
    hold on;
    plot(link_length1 * cos(theta1) + link_length2 * cos(theta1 + theta2), link_length1 * sin(theta1) + link_length2 * sin(theta1 + theta2), 'r', 'LineWidth', 2);
    
    plot(o1(1), o1(2), 'gx', 'LineWidth', 2);
    plot(o2(1), o2(2), 'rx', 'LineWidth', 2);
    
    
    drawnow;
    plot([0, o1(1), o2(1)], [0, o1(2), o2(2)])
    axis([-1, 2, 0, 2]);
    
end

    % Extract the individual theta1 and theta2 configurations for plotting
    theta1_configurations = robot_configurations(1,:);
    theta2_configurations = robot_configurations(2,:);
    
    % Plot the robot's final path
    plot(link_length1 * cos(theta1_configurations),link_length1 * sin(theta1_configurations),'g','LineWidth', 2);
    plot(link_length1 * cos(theta1_configurations) + link_length2 * cos(theta1_configurations + theta2_configurations), link_length1 * sin(theta1_configurations) + link_length2 * sin(theta1_configurations + theta2_configurations), 'b', 'LineWidth', 2);
    hold on;
    plot(o1_initial(1), o1_initial(2), 'go', 'LineWidth', 2);
    plot(o2_initial(1), o2_initial(2), 'bo', 'LineWidth', 2);
    plot(o1_final(1), o1_final(2), 'g*', 'LineWidth', 2);
    plot(o2_final(1), o2_final(2), 'b*', 'LineWidth', 2);
    plot(obstacle_x, obstacle_y, 'ko', 'LineWidth', 2);
    xlabel('X');
    ylabel('Y');
    title('Robot Path with Obstacle');
  
    hold off;
    
    % Print the theta1 and theta2 configurations throughout the path
    fprintf('Theta1 configurations: ');
    fprintf('%.2f ', theta1_configurations);
    fprintf('\nTheta2 configurations: ');
    fprintf('%.2f ', theta2_configurations);
    fprintf('\n');










